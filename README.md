# JsonDict

A small Javascript code to change the language of a page without reloading.

## Usage
`dict/fr.json` :
```json
    {
        "title": {
            "one": "Premier titre",
            "two": "Second titre"
        },

        "hello": "Bonjour"
    }
```

`dict/en.json` :
```json
    {
        "title": {
            "one": "First title",
            "two": "Second title"
        },

        "hello": "Hello %%"
    }
```

`index.html` :
```html
    <!DOCTYPE html>
    <html>
        <body>
            <h1 class="jdict" data-jdict="title.one"></h1>
            <h2 class="jdict" data-jdict="title.two"></h2>

            <p class="jdict" data-jdict="hello" data-jdict-r="Bernard"></p>

            <script type="text/javascript" src="jdict.js"></script>
            <script>

// Load French then load English 3 secondes after
JDict.load("dict/fr.json");

setTimeout(()=> {
    JDict.load("dict/en.json");
}, 3000);
            </script>
    </html>
```

## Language chooser and load from URL
```html
<!DOCTYPE html>
<html>
    <body>
        <ul id="lang">
            <li id="fr" class="selected">Français</li>
            <li id="en">English</li>
        </ul>

        <script type="text/javascript">
// Click handler
let langs = document.getElementById("lang").getElementsByTagName("li");
let langClick = (e) => {
	for (var i = langs.length - 1; i >= 0; i--)
		langs[i].classList.remove("selected");
	
	e.currentTarget.classList.add("selected");
	JDict.load(`dict/${e.currentTarget.id}.json`);


	let url = new URL(document.location);
	url.searchParams.set("lang", e.currentTarget.id))
	window.history.pushState({"pageTitle": document.title},"", url.href);
};

for (var i = langs.length - 1; i >= 0; i--)
	langs[i].addEventListener("click", langClick);

// Load
let url = new URL(document.location);
let lang = url.searchParams.get("lang");
let langElem = document.getElementById(`${lang}`);

if (langElem == null)
	JDict.load("dict/fr.json"); // defaults
else
	langElem.click();


        </script>
    </body>
</html>
```
