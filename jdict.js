// Json Dictionary
// BARDON Nathan <dev@lazyfox.fr>
// 18 july 2021

var JDict = {
    dict: null,

    load: (path, updateTextOnDone = true, disableCache = true) => {
        let xhr = new XMLHttpRequest();

        xhr.onreadystatechange = (e) => {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                JDict.dict = JSON.parse(xhr.responseText);

                if (updateTextOnDone)
                    JDict.updateAll();
            }
        };

        if (disableCache)
            path += (path.indexOf("?") == -1 ? "?": "&") + Date.now();
        

        xhr.open("GET", path);
        xhr.send();
    },




	updateAll: () => {
		if (JDict.dict == null) {
			console.error("Please call JDict.load(dictPath) before");
			return;
		}

        var elems = document.getElementsByClassName("jdict");
        for (var i = elems.length - 1; i >= 0; i--)
			JDict.updateElement(elems[i]);
	},




	updateElement: (elem) => {
        if (JDict.dict == null) {
            console.error("Please call JDict.load(dictPath) before");
            return;
        }

        var id = elem.getAttribute("data-jdict");
        if (id == null)
            return;

        // Find text in JSON tree
        var e = JDict.dict;
        var subs = id.split(".");
        for (var j = 0; j < subs.length; j++) {
            if (!e.hasOwnProperty(subs[j])) {
                console.error("Unable to find JDict id ", id, elem);
                return;
            }

            e = e[subs[j]];
        }

		if (typeof(e) != "string") {
			console.error("JDict id is not a string ", id, elem);
			return;
		}

        if (e.indexOf("%%") != -1 && elem.hasAttribute("data-jdict-r"))
            e = e.replace(/%%/g, elem.getAttribute("data-jdict-r"));

        elem.innerHTML = e;
    }
};
